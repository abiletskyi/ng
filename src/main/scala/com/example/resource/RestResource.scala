package com.example.resource

import javax.servlet.ServletContext
import javax.ws.rs._
import javax.ws.rs.core.{Context, MediaType}

import com.example.cache.{F1DataAccess, F2DataAccess}
import com.example.model.Model.{GetResponse, PostRequest, PostResponse}

@Path("/application")
class RestResource {

  @Context private val context: ServletContext = null

  // probably might use AsyncContext in GET/POST

  @GET
  @Path("/computation")
  @Produces(Array(MediaType.APPLICATION_XML))
  def getValue(@QueryParam("v1") v1: Int) = {
    val f2DataAccess = context.getAttribute("f2ReadWriteCache").asInstanceOf[F2DataAccess]

    GetResponse(
      f2DataAccess.get(v1) match {
        case Some(v) =>
          (if (v > 10) v - 10 else v).toString
        case None => "null"
      })
  }

  @POST
  @Path("/computation")
  @Produces(Array(MediaType.APPLICATION_XML))
  def updateValue(postRequest: PostRequest) = {
    val f1DataAccess = context.getAttribute("f1ReadOnlyCache").asInstanceOf[F1DataAccess]
    val f2DataAccess = context.getAttribute("f2ReadWriteCache").asInstanceOf[F2DataAccess]

    val aux = f1DataAccess.get(postRequest.v3).map(_ + postRequest.v2)
    aux match {
      case Some(v) =>
        if (v < 10) {
          f2DataAccess.update(postRequest.v4, v + 10)
          PostResponse(0)
        } else {
          f2DataAccess.update(postRequest.v4, v)
          PostResponse(1)
        }
      case None =>
        // consider it as condition didn't hold - return 1
        PostResponse(1)
    }
  }
}