package com.example.server

import java.io.File
import java.util.concurrent.{ExecutorService, ThreadPoolExecutor, TimeUnit, Executors}
import javax.servlet.ServletContextEvent

import com.example.cache.{F2DataAccess, F1DataAccess}
import net.sf.ehcache.CacheManager
import org.jboss.resteasy.plugins.server.servlet.ResteasyBootstrap

class CustomRestEasyBootstrap extends ResteasyBootstrap {
  override def contextInitialized(event: ServletContextEvent): Unit = {
    val context = event.getServletContext

    val threadExecutor = Executors.newSingleThreadExecutor()

    val f1File = context.getInitParameter("com.example.cache.f1.file")
    val f2File = context.getInitParameter("com.example.cache.f2.file")
    val f2MaxEnries = context.getInitParameter("com.example.cache.f2.maxEntries")

    val manager = CacheManager.create()
    context.setAttribute("syncThreadExecutor", threadExecutor)
    context.setAttribute("f1ReadOnlyCache", new F1DataAccess(manager.getCache("f1ReadOnlyCache"), new File(f1File)))
    context.setAttribute("f2ReadWriteCache", new F2DataAccess(manager.getCache("f2ReadWriteCache"),  new File(f2File),
      threadExecutor, f2MaxEnries.toInt))
    context.setAttribute("ehcacheManager", manager)

    super.contextInitialized(event)
  }

  override def contextDestroyed(event: ServletContextEvent): Unit = {
    val threadExecutor = event.getServletContext.getAttribute("syncThreadExecutor").asInstanceOf[ExecutorService]
    threadExecutor.awaitTermination(1, TimeUnit.MINUTES)

    event.getServletContext.getAttribute("syncThreadExecutor").asInstanceOf[CacheManager].shutdown()
    super.contextDestroyed(event)
  }
}
