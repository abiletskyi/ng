package com.example.csv

import java.io.File

/**
 * @author Andrii Biletskyi
 */
trait IndexedCsvReader {
  val file: File

  def read(pos: Int): Option[String]

  def readAll(): Iterator[String]

  def close(): Unit
}
