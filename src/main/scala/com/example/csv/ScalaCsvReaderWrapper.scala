package com.example.csv

import java.io.File

import com.github.tototoshi.csv.CSVReader

import scala.util.Try

/**
 * A simple wrapper on CSVReader. Taking into account our case restrictions, it might be much more efficient
 * to use RandomAccessFile and cache each x-th entry byte-offset in File
 *
 * @author Andrii Biletskyi
 */
class ScalaCsvReaderWrapper(override val file: File) extends IndexedCsvReader {

  private val reader = CSVReader.open(file)

  override def read(pos: Int): Option[String] = {
    readAll().drop(pos).take(1).toList.headOption
  }

  override def readAll(): Iterator[String] = {
    reader.iterator.flatten
  }

  override def close() {
    Try(reader.close())
  }
}
