package com.example.model

import javax.xml.bind.annotation.{XmlAccessType, XmlAccessorType, XmlRootElement}

/**
 * @author Andrii Biletskyi
 */
object Model {

  @XmlRootElement(name = "getResponse")
  @XmlAccessorType(XmlAccessType.FIELD)
  case class GetResponse(result: String) {
    def this() = this("null")
  }

  @XmlRootElement(name = "postRequest")
  @XmlAccessorType(XmlAccessType.FIELD)
  case class PostRequest(v2: Double, v3: Int, v4: Int) {
    def this() = this(0, 0, 0)
  }

  @XmlRootElement(name = "postResponse")
  @XmlAccessorType(XmlAccessType.FIELD)
  case class PostResponse(result: Int) {
    def this() = this(0)
  }

}

