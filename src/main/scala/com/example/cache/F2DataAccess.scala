package com.example.cache

import java.io.File
import java.nio.file.StandardCopyOption
import java.util.concurrent.ExecutorService
import java.util.concurrent.locks.{ReadWriteLock, ReentrantLock, ReentrantReadWriteLock}

import com.example.csv.{IndexedCsvReader, ScalaCsvReaderWrapper}
import com.github.tototoshi.csv.CSVWriter
import net.sf.ehcache.constructs.blocking.SelfPopulatingCache
import net.sf.ehcache.{Ehcache, Element}
import org.slf4j.LoggerFactory

import scala.collection.mutable
import scala.util.Try


/**
 * A wrapper around "non-expirable" Ehcache cache with manual (custom) expiration and backup control.
 * Due to cache data-source inefficiency (a File on local file system) neither write-through nor write-behind
 * models are acceptable.
 *
 * An expiration model is very simple (and not quite efficient) once cache if filled up to maxCacheEntries
 * cache-sync is started after which cache is cleared.
 *
 * The contract between read-write-sync operations is the following:
 * - reads-writes are not not blocking each other
 * - reads and writes block cache sync
 * - sync blocks writes only while cache is being copied to in memory map
 * - sync block reads during the whole sync-procedure
 *
 * @author Andrii Biletskyi
 */
class F2DataAccess(baseCache: Ehcache, file: File, threadExecutor: ExecutorService, maxCacheEntries: Int = 10000) {

  private lazy val log = LoggerFactory.getLogger(classOf[F2DataAccess])

  /**
   * In our case it's the other way round. 'update' threads obtain read lock - simultaneous update operations are safe
   * since EhCache is used; write lock is obtained whenever copying and clearing cache is to be performed -
   * should be exclusive as write lock is in ReadWriteLock, also write lock cannot be obtained if other thread updates cache
   */
  private val updateBackupLock: ReadWriteLock = new ReentrantReadWriteLock(true)

  /**
   * A lock for read element - backup cache pairs of operations. Simultaneous reads are allowed so long as backup
   * and clearing cache operations doesn't start (respective lock wasn't obtained)
   */
  private val readBackupLock: ReadWriteLock = new ReentrantReadWriteLock(true)

  /**
   * Lots of locks...
   * This locks prevents simultaneous syncCache operations as this is very time-consuming and
   * should be exclusive
   */
  private val dumpCacheLock = new ReentrantLock()

  protected def createCsvReader(file: File): IndexedCsvReader = new ScalaCsvReaderWrapper(file)

  private[this] val cache: Ehcache = new SelfPopulatingCache(baseCache, new CsvCacheEntryFactory(createCsvReader(file)))

  // here we will stick to CSVWriter, no need to create wrappers
  protected def createCsvWriter(file: File) = CSVWriter.open(file)

  def update(key: Int, value: Double) {
    log.debug(s"F2Cache: Updating key $key with value: $value")
    updateBackupLock.readLock().lock()
    try {
      cache.put(new Element(key, Some(value)))
      if (cache.getKeys.size() > maxCacheEntries) {
        log.debug("F2Cache size is {}, trying to dump cache", cache.getKeys.size())
        trySyncCache()
      }
    } finally {
      updateBackupLock.readLock().unlock()
    }
    log.debug(s"F2Cache: Updated key $key with value: $value")
  }

  def get(key: Int): Option[Double] = {
    readBackupLock.readLock().lock()
    try {
      val value = cache.get(key).getObjectValue.asInstanceOf[Option[Double]]
      log.debug(s"F2Cache value for $key: $value")
      if (cache.getKeys.size() > maxCacheEntries) {
        log.debug("F2Cache size is {}, trying to dump cache", cache.getKeys.size())
        trySyncCache()
      }
      // if wasn't able to populate data return None but retry next time
      if (value == null) None else value
    } finally {
      readBackupLock.readLock().unlock()
    }
  }

  /**
   * Tries to schedule cache dump, if already in progress suppresses execution
   */
  def trySyncCache() {
    threadExecutor.submit(new Runnable {
      override def run() {
        if (dumpCacheLock.tryLock()) {
          try
            syncCache()
          catch {
            case e: Exception => log.error("Failed to dump cache", e)
          }
          finally dumpCacheLock.unlock()
        }

      }
    })
  }

  /**
   * Sync in-memory cache and file
   */
  private[this] def syncCache() {
    import scala.collection.JavaConversions._

    log.info("Starting F2 cache dump to file")
    // 1. Forbid all cache reads/updates
    updateBackupLock.writeLock().lock()
    readBackupLock.writeLock().lock()

    val cacheCopy = new mutable.HashMap[Int, Double]()
    try {
      // 2. Copy the cache and clear it
      log.trace("Copying and clearing cache")
      for (key <- cache.getKeys.toList) {
        cache.get(key).getObjectValue.asInstanceOf[Option[Double]].foreach {
          cacheValue =>
            cacheCopy.put(key.asInstanceOf[Int], cacheValue)
        }
      }

      // 3. Clear cache to release the memory
      cache.removeAll(true)
    } finally {
      // allow updates again, all data is backuped, reads are still forbidden because
      // all of them will be 'miss' and will require file read
      updateBackupLock.writeLock().unlock()
    }

    // 4. Update file taking with cache copy
    try {
      flushCache(cacheCopy.toMap)
    } finally {
      // base file is updated and consistent, reads may be performed
      readBackupLock.writeLock().unlock()
    }
    log.info("Completed F2 cache dump to file")
  }

  /**
   * Renew base file with updated values that are present in the cacheCopy.
   *
   * @param cacheCopy storage with some updated values
   */
  private[this] def flushCache(cacheCopy: Map[Int, Double]): Unit = {
    val filename = file.getAbsolutePath
    val tempFile = File.createTempFile("cache-tempfile", ".tmp", file.getParentFile)
    val tempFilename = tempFile.getAbsolutePath

    var tempCsvWritter: CSVWriter = null
    var tempCsvReader: IndexedCsvReader = null
    try {
      tempCsvWritter = createCsvWriter(tempFile)
      tempCsvReader = createCsvReader(file)

      log.trace("Creating updated cache file")

      val oldNumbersTry = Try(tempCsvReader.readAll())
      oldNumbersTry.foreach(old => mergeFileAndCache(old, cacheCopy, tempCsvWritter))

      tempCsvReader.close()
      tempCsvWritter.close()
      java.nio.file.Files.move(tempFile.toPath, file.toPath, StandardCopyOption.REPLACE_EXISTING)
    } finally {
      if (tempCsvWritter ne null) {
        tempCsvWritter.flush()
        tempCsvWritter.close()
      }
      if (tempCsvReader ne null)
        tempCsvReader.close()
    }
  }

  private def mergeFileAndCache(fileEntries: Iterator[String], cacheCopy: Map[Int, Double], csvWriter: CSVWriter): Unit = {
    import com.example.cache.F2DataAccess.EntriesPerLine

    // merge existing file entries and cache copy; a bit messy because we want to make it lazy,
    // not to pull in the whole file
    val updatedNumbers =
      fileEntries.zipWithIndex.map {
        case (oldNumber, pos) =>
          (pos, cacheCopy.getOrElse(pos, oldNumber))
      }

    var lastExistingPos = 0
    updatedNumbers.grouped(EntriesPerLine).foreach {
      line =>
        csvWriter.writeRow(line.map { case (pos, value) => value})
        lastExistingPos = line.lastOption.map(_._1).getOrElse(0)
    }
    val tailStart = lastExistingPos + 1

    val appendedElements = cacheCopy.filterKeys(pos => pos > tailStart)
    if (appendedElements.size > 0) {
      tailStart.to(appendedElements.maxBy(_._1)._1).map {
        case pos =>
          appendedElements.getOrElse(pos, "")
      }.grouped(EntriesPerLine).foreach {
        line =>
          csvWriter.writeRow(line)
      }
    }

  }
}

object F2DataAccess {
  val EntriesPerLine = 10
}
