package com.example.cache

import com.example.csv.IndexedCsvReader
import net.sf.ehcache.constructs.blocking.CacheEntryFactory

import scala.util.{Failure, Success, Try}

/**
 * Gets data with IndexedCsvReader, stores Double entries.
 * On reading failure entry is not created (to be retried next time), on parsing error
 * an element with None value is created.
 *
 * @author Andrii Biletskyi
 */
class CsvCacheEntryFactory(csvReaderCreator: => IndexedCsvReader) extends CacheEntryFactory{
  override def createEntry(key: Object): Object = {
    // createEntry must be thread-safe
    val csvReader: IndexedCsvReader = csvReaderCreator

    val value =
      Try(csvReader.read(key.asInstanceOf[Int])) match {
        case Success(valueRead) => valueRead.flatMap(entry => Option(Try(entry.toDouble).getOrElse(null)))
        case Failure(t) => null  // in order to retry next time
      }
    if (csvReader ne null)
      Try(csvReader.close())

    value
  }
}
