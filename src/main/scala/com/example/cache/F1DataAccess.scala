package com.example.cache

import java.io.File

import com.example.csv.{IndexedCsvReader, ScalaCsvReaderWrapper}
import com.example.resource.RestResource
import net.sf.ehcache.Ehcache
import net.sf.ehcache.constructs.blocking.SelfPopulatingCache
import org.slf4j.LoggerFactory


/**
 * A simple wrapper around self populating read-only LFU cache with key Int (position in file) and value - Option[Double]
 * (None if value is absent at this position or failed to parse)
 *
 * @author Andrii Biletskyi
 */
class F1DataAccess(baseCache: Ehcache, file: File) {

  private lazy val log = LoggerFactory.getLogger(classOf[F1DataAccess])

  def createCsvReader(file: File): IndexedCsvReader = new ScalaCsvReaderWrapper(file)

  private[this] val cache: Ehcache = new SelfPopulatingCache(baseCache, new CsvCacheEntryFactory(createCsvReader(file)))

  def get(key: Int): Option[Double] = {
    val value = cache.get(key).getObjectValue.asInstanceOf[Option[Double]]
    log.debug(s"F1Cache value for $key: $value")
    // if wasn't able to populate data return None but retry next time
    if (value == null) None else value
  }
}