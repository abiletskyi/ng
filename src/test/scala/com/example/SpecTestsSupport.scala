package com.example

import com.example.csv.IndexedCsvReader
import org.mockito.Mockito._

/**
 * @author Andrii Biletskyi
 */
trait SpecTestsSupport {
  def indexedCsvReaderMock(content: Map[Int, Option[String]]) = {
    val csvReader = mock(classOf[IndexedCsvReader])
    for ((k, v) <- content)
      when(csvReader.read(k)).thenReturn(v)
    csvReader
  }
}
