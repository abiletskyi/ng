package com.example.cache

import com.example.SpecTestsSupport
import com.example.csv.IndexedCsvReader
import org.mockito.Matchers.{anyInt, eq => meq}
import org.mockito.Mockito.{mock, when}
import org.specs2.mutable._

/**
 * @author Andrii Biletskyi
 */
class CsvCacheEntryFactorySpec extends Specification with SpecTestsSupport {
  sequential

  "CsvCacheEntryFactory" should {
    "get data from the underlying reader" in {
      def csvReaderCreator = indexedCsvReaderMock(Map(1 -> Some("2.1")))

      val factoryUnderTest = new CsvCacheEntryFactory(csvReaderCreator)
      factoryUnderTest.createEntry(1.asInstanceOf[Object]) should_== Some(2.1)
    }

    "return null upon read failures" in {
      val csvReader = mock(classOf[IndexedCsvReader])
      when(csvReader.read(anyInt)).thenThrow(new RuntimeException("expected"))
      def csvReaderCreator = csvReader

      val factoryUnderTest = new CsvCacheEntryFactory(csvReaderCreator)
      factoryUnderTest.createEntry(1.asInstanceOf[Object]) should_== null
    }

    "transform empty entries to None" in {
      def csvReaderCreator = indexedCsvReaderMock(Map(1 -> Some("")))

      val factoryUnderTest = new CsvCacheEntryFactory(csvReaderCreator)
      factoryUnderTest.createEntry(1.asInstanceOf[Object]) should_== None
    }
  }
}