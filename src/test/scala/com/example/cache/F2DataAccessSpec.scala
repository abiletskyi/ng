package com.example.cache

import java.io.{File, FileWriter, Writer}
import java.util.concurrent.{ExecutorService, Executors}

import com.example.SpecTestsSupport
import com.example.csv.IndexedCsvReader
import com.github.tototoshi.csv.CSVWriter
import net.sf.ehcache.config.PersistenceConfiguration.Strategy
import net.sf.ehcache.config.{CacheConfiguration, PersistenceConfiguration}
import net.sf.ehcache.store.MemoryStoreEvictionPolicy
import net.sf.ehcache.{Cache, CacheManager, Ehcache}
import org.mockito.Matchers.{any, anyInt, eq => meq}
import org.mockito.Mockito.{mock, verify, when, times}
import org.specs2.mutable._

import scala.collection.mutable.ListBuffer


/**
 * @author Andrii Biletskyi
 */
class F2DataAccessSpec extends Specification with SpecTestsSupport {
  sequential

  def createTestF2DataAccess(baseCache: Ehcache, file: File, threadExecutor: ExecutorService,
                             createCsvReaderFunc: => IndexedCsvReader,
                             maxCacheEntries: Int = 10000): F2DataAccess = {
    new F2DataAccess(baseCache, file, threadExecutor, maxCacheEntries) {
      override def createCsvReader(file: File) = createCsvReaderFunc
    }
  }

  def createTestSyncF2DataAccess(baseCache: Ehcache, file: File, threadExecutor: ExecutorService,
                                 createCsvReaderFunc: => IndexedCsvReader,
                                 createCsvWriterFunc: => CSVWriter,
                                 maxCacheEntries: Int = 10000): F2DataAccess = {
    new F2DataAccess(baseCache, file, threadExecutor, maxCacheEntries) {
      override def createCsvReader(file: File) = createCsvReaderFunc

      override def createCsvWriter(file: File) = createCsvWriterFunc
    }
  }

  class CSVWriterStub(writer: Writer = new FileWriter("temp.txt")) extends CSVWriter(writer) {
    val entries = new ListBuffer[Any]

    override def close() {}

    override def writeAll(allLines: Seq[Seq[Any]]) {
      entries.appendAll(allLines.flatten)
    }

    override def writeRow(fiedls: Seq[Any]) {
      entries.appendAll(fiedls)
    }

  }

  "F2DataAccess" should {
    "be self populating" in new contextF2 {
      val csvReader = indexedCsvReaderMock(Map(1 -> Some("2.1")))
      val f2UnderTest = createTestF2DataAccess(testCache, new File(""), null, csvReader)
      f2UnderTest.get(1) should_== Some(2.1d)

      verify(csvReader).read(meq(1))
    }

    "be based on cache" in new contextF2 {
      val csvReader = indexedCsvReaderMock(Map(1 -> Some("2.1")))
      val f2UnderTest = createTestF2DataAccess(testCache, new File(""), null, csvReader)
      f2UnderTest.get(1) should_== Some(2.1d)
      f2UnderTest.get(1) should_== Some(2.1d)

      verify(csvReader, times(1)).read(meq(1))
    }

    "tolerate read file failures" in new contextF2 {
      val csvReader = mock(classOf[IndexedCsvReader])
      when(csvReader.read(1)).thenThrow(new RuntimeException("expected")).thenReturn(Some("2.1"))
      val f2UnderTest = createTestF2DataAccess(testCache, new File(""), null, csvReader)
      f2UnderTest.get(1) should_== None
      f2UnderTest.get(1) should_== Some(2.1d)

      verify(csvReader, times(2)).read(meq(1))
    }

    "be read-write cache" in new contextF2 {
      val csvReader = indexedCsvReaderMock(Map(1 -> Some("1.1"), 2 -> Some("-2.2")))
      val f2UnderTest = createTestF2DataAccess(testCache, new File(""), null, csvReader)

      f2UnderTest.get(1) should_== Some(1.1)
      f2UnderTest.update(1, -5.5)
      f2UnderTest.get(1) should_== Some(-5.5)
    }

    "start backup process once base cache is filled" in new context {
      val csvReader = mock(classOf[IndexedCsvReader])
      val threadExecutor = mock(classOf[ExecutorService])
      when(csvReader.read(anyInt)).thenReturn(Some("2.1"))
      val f2UnderTest = createTestF2DataAccess(testCache, new File(""), threadExecutor, csvReader, 2)
      f2UnderTest.update(1, 1.1)
      f2UnderTest.update(2, 1.2)
      f2UnderTest.update(3, 1.2)
      verify(threadExecutor).submit(any(classOf[Runnable]))
    }

    "update file with cached values once trySync is done" in new context {
      val csvReader = mock(classOf[IndexedCsvReader])
      val csvWriter = new CSVWriterStub()
      val threadExecutor = Executors.newSingleThreadExecutor()
      val testFile = mock(classOf[File])
      when(csvReader.readAll()).thenReturn(List("", "-1.1", "2.2", "", "-4.4").iterator)

      val f2UnderTest = createTestSyncF2DataAccess(testCache, testFile, threadExecutor, csvReader, csvWriter, 10)
      f2UnderTest.update(1, 1.1)
      f2UnderTest.update(2, 1.2)
      f2UnderTest.update(3, 1.3)
      f2UnderTest.update(6, 777)

      f2UnderTest.trySyncCache()
      Thread.sleep(1000)

      testCache.getKeys.size() should_== 0
      csvWriter.entries.map(_.toString).toList should_== List("", "1.1", "1.2", "1.3", "-4.4", "", "777.0")
    }
  }
}

object beforeClassF2 {
  lazy val manager = CacheManager.create()
}

trait contextF2 extends BeforeAfter {

  import com.example.cache.beforeClass.manager

  val testCache = new Cache(
    new CacheConfiguration("testCacheF2", 1000)
      .memoryStoreEvictionPolicy(MemoryStoreEvictionPolicy.LFU)
      .eternal(true)
      .persistence(new PersistenceConfiguration().strategy(Strategy.NONE)))

  override def before {
    manager.addCache(testCache)
  }

  override def after {
    testCache.removeAll()
    manager.removeCache("testCacheF2")
  }
}
