package com.example.cache

import java.io.File
import java.util.concurrent.ExecutorService

import com.example.SpecTestsSupport
import com.example.csv.IndexedCsvReader
import net.sf.ehcache.config.PersistenceConfiguration.Strategy
import net.sf.ehcache.config.{CacheConfiguration, PersistenceConfiguration}
import net.sf.ehcache.store.MemoryStoreEvictionPolicy
import net.sf.ehcache.{Cache, CacheManager, Ehcache}
import org.mockito.Matchers.{eq => meq}
import org.mockito.Mockito.{mock, verify, when, times}
import org.specs2.mutable._

/**
 * @author Andrii Biletskyi
 */
class F1DataAccessSpec extends Specification with SpecTestsSupport {
  sequential

  def createTestF1DataAccess(baseCache: Ehcache, file: File, threadExecutor: ExecutorService,
                             createCsvReaderFunc: => IndexedCsvReader): F1DataAccess = {
    new F1DataAccess(baseCache, file) {
      override def createCsvReader(file: File) = createCsvReaderFunc
    }
  }

  "F1DataAccess" should {
    "be self populating" in new context {
      val csvReader = indexedCsvReaderMock(Map(1 -> Some("1.1"), 2 -> Some("-2.2")))
      val f1InderTest = createTestF1DataAccess(testCache, new File(""), null, csvReader)
      f1InderTest.get(2) should_== Some(-2.2d)

      verify(csvReader).read(meq(2))
    }

    "be based on cache" in new context {
      val csvReader = indexedCsvReaderMock(Map(1 -> Some("1.1"), 2 -> Some("-2.2")))
      val f1InderTest = createTestF1DataAccess(testCache, new File(""), null, csvReader)
      f1InderTest.get(1) should_== Some(1.1d)
      f1InderTest.get(1) should_== Some(1.1d)

      verify(csvReader, times(1)).read(meq(1))
    }

    "tolerate read file failures" in new context {
      val csvReader = mock(classOf[IndexedCsvReader])
      when(csvReader.read(1)).thenThrow(new RuntimeException("expected")).thenReturn(Some("2.1"))
      val f1InderTest = createTestF1DataAccess(testCache, new File(""), null, csvReader)
      f1InderTest.get(1) should_== None
      f1InderTest.get(1) should_== Some(2.1d)

      verify(csvReader,times(2)).read(meq(1))
    }
  }
}

object beforeClass {
  lazy val manager = CacheManager.create()
}

trait context extends BeforeAfter {

  import com.example.cache.beforeClass.manager

  val testCache = new Cache(
    new CacheConfiguration("testCacheF1", 1000)
      .memoryStoreEvictionPolicy(MemoryStoreEvictionPolicy.LFU)
      .eternal(true)
      .persistence(new PersistenceConfiguration().strategy(Strategy.NONE)))

  override def before {
    manager.addCache(testCache)
  }

  override def after {
    testCache.removeAll()
    manager.removeCache("testCacheF1")
  }
}
