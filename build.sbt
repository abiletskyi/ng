name := "ng-test-assignment"

scalaVersion := "2.10.3"

version := "1.0"

jetty()

libraryDependencies ++= Seq(
  "javax.ws.rs" % "javax.ws.rs-api" % "2.0",
  "org.jboss.resteasy" % "resteasy-jaxrs" % "3.0.8.Final" exclude("org.slf4j", "slf4j-simple"),
  "org.jboss.resteasy" % "resteasy-jaxb-provider" % "3.0.8.Final",
  "org.eclipse.jetty" % "jetty-webapp" % "9.0.5.v20130815" % "container",
  "org.eclipse.jetty.orbit" % "javax.servlet" % "3.0.0.v201112011016" % "container,compile" artifacts Artifact("javax.servlet", "jar", "jar"),
  "ch.qos.logback" % "logback-classic" % "1.0.13",
  "org.slf4j" % "log4j-over-slf4j" % "1.7.5",
  "net.sf.ehcache" % "ehcache" % "2.8.2",
  "javax.transaction" % "jta" % "1.1",
  "com.github.tototoshi" %% "scala-csv" % "1.0.0",
  "org.slf4j" % "slf4j-api" % "1.7.7",
  "org.specs2" %% "specs2" % "2.3.4" % "test",
  "org.mockito" % "mockito-all" % "1.9.5" % "test",
  "org.scalatest" %% "scalatest" % "1.9.1" % "test",
  "ch.qos.logback" % "logback-core" % "1.0.13",
  "ch.qos.logback" % "logback-classic" % "1.0.13"
)
